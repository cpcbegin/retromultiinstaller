# Multi retro utils installer
Script to install more common retro computers emulators and tools in GNU/Linux (Debian/Ubuntu family distros).
There will be a full operative shortcut in menu for each graphic software you install.

> ADVICE: THIS SCRIPT DOESN'T INCLUDE ANY ILLEGAL ROM, BIOS OR ANY QUESTIONABLE SOFTWARE.

You can see the menus on English or Spanish depending of your locale configuration.



## Emulators included
### Amstrad
- [ACE-DL](http://www.roudoudou.com/ACE-DL/): Amstrad CPC emulator. ![Pure binary](./resources/images/icons/binary.png)
- [Arnold (by Rofl0r)](https://github.com/rofl0r/arnold): Amstrad CPC emulator (Rofl0r's fork of Arnold). ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Caprice 32 (by Colin Pitrat)](https://github.com/ColinPitrat/caprice32): Amstrad CPC emulator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [CPCEC-GTK](https://bitbucket.org/norecess464/cpcec-gtk/src/master/): Amstrad CPC emulador, CPCEC fork focused in development. ![From package](./resources/images/icons/package.png)
- [CPCEmu 2.0](https://cpc-emu.org): Amstrad CPC emulator, the classic for new systems. ![Pure binary](./resources/images/icons/binary.png) ![Raspian 32 compatible](./resources/images/icons/raspbian.png)
- [CPCEPower](https://www.cpcwiki.eu/forum/emulators/cpcepower-v2101/msg197261): Amstrad CPC emulator. ![Pure binary](./resources/images/icons/binary.png) ![Raspian 32 compatible](./resources/images/icons/raspbian.png)
- [Java CPC emulator](http://cpc-live.com/data/): Amstrad CPC emulator based in java. ![Pure java](./resources/images/icons/java.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Joyce PCW/Anne emulator (by John Elliott)](http://www.seasip.info/Unix/Joyce): Amstrad PCW emulator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [WinAPE](http://www.winape.net/): Amstrad CPC emulator for Windows (runs with wine). Very advanced for programming. ![Runs with wine](./resources/images/icons/wine.png)
- [xCPC emulator](https://www.xcpc-emulator.net/): Amstrad CPC emulator for Linux, BSD, Unix. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### Apple
- [BasiliskII](https://basilisk.cebix.net/): Macintosh emulator (focused on OSX 7 or 8) ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [LinApple](https://github.com/linappleii/linapple): Apple II emulator ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### Atari ST
- [Hatari](https://hatari.tuxfamily.org/): Atari ST emulator ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### Commodore
- [FS UAE](https://fs-uae.net): Commodore Amiga emulator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [VICE](http://vice-emu.sourceforge.net/): Commodore computers emulator (not Amiga). ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### MSX
- [OpenMSX](https://openmsx.org/): MSX emulator ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### ZX Spectrum
- [BasinC](https://arda.kisafilm.org/blog/): ZX Spectrum emulator for developers. ![Runs with wine](./resources/images/icons/wine.png)
- [FBZX](https://rastersoft.com/programas/fbzx.html): ZX Spectrum emulator. ![From package](./resources/images/icons/package.png)
- [FUSE](http://fuse-emulator.sourceforge.net/): ZX Spectrum emulator. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Kosarev ZX Spectrum Emulator (python)](https://github.com/kosarev/zx): ZX Spectrum emulator (focused on soviet machines). ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [SpectEmu](https://github.com/radekp/spectemu): ZX Spectrum emulator for GNU/Linux & other unixes. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [ZXPoly](https://github.com/raydac/zxpoly): Rare ZX Spectrum emulator. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### Other 8-bits computers
- [Regnecentralen RC700 Emulator](http://www.jbox.dk/rc702/emulator.shtm): RC700  emulator ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [sQLux](https://github.com/SinclairQL/sQLux). QL emulator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [sz81](http://sz81.sourceforge.net). ZX 81 Emulador. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [XRoar](https://www.6809.org.uk/xroar/): Tandy CoCo & Dragon computers emulator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### Arcade
- [Cannonball](https://github.com/djyt/cannonball): Out Run emulator. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [MAME](https://mame.net) ![From package](./resources/images/icons/package.png): Arcade machines emulator, legendary. ![Raspian compatible](./resources/images/icons/raspbian.png)


### Consoles
- [DesmuME](http://desmume.org/): Nintendo DS. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Dolphin](https://github.com/dolphin-emu/dolphin): WII & Game Cube emulator. ![From package](./resources/images/icons/package.png)
- [DuckStation](https://github.com/stenzek/duckstation): Playstation. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [GearColeco](https://github.com/drhelius/Gearcoleco): Coleco. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Mednafen](https://mednafen.github.io/): Several consoles emulator. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Osmose](https://segaretro.org/Osmose): Several consoles emulator. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [PCSXR](https://github.com/iCatButler/pcsxr): Playstation. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [PCSX2](https://pcsx2.net/): Playstation II ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Redream](https://redream.io/): DreamCast emulator. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [ReiCast](https://github.com/reicast/reicast-emulator/blob/alpha/README.md): DreamCast emulator. ![From package](./resources/images/icons/package.png)
- [Stella](https://stella-emu.github.io/): Atari 2600. ![From package](./resources/images/icons/package.png)
- [Virtual Jaguar](https://icculus.org/virtualjaguar/): Atari Jaguar emulator ![From package](./resources/images/icons/package.png)
- [ZSNES](http://www.zsnes.com/): Supernintendo emulator ![From package](./resources/images/icons/package.png)


### Several systems
- [Clock Signal](https://github.com/TomHarte/CLK): Several 8 bits computers and consoles emulator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [CPCEC/ZXSEC](http://cngsoft.no-ip.org/cpcec.htm): Amstrad CPC & ZX Spectrum emulator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Higan](https://byuu.org/higan/): Several 8 bits computers and consoles emulator. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [IBMulator](https://github.com/barotto/IBMulator): IBM PC emulator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [PCEM](https://pcem-emulator.co.uk): Several classic PCs emulator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Retropie](https://retropie.org.uk/): Retrocomputing emulator system. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Retro Virtual Machine (by Juan Carlos González Amestoy)](https://www.retrovirtualmachine.org/): Amstrad CPC & ZX Spectrum emulator. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian64.png)
- [ZEsarUX (by Ceśar Hernández Bañó)](https://github.com/chernandezba/zesarux): 8bit computers emulator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### Just OS emulators
- [Dosbox](https://www.dosbox.com/): DOS systems emulator. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [WINE](https://www.winehq.org/): Windows instruction interpreter. ![From package](./resources/images/icons/package.png)
- [ANSI CP/M](https://github.com/z88dk/cpm): CP/M systems emulator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [IZ CP/M](https://github.com/ivanizag/iz-cpm): CP/M systems emulator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)



## Games
### Power games (you must play)
- [Game Play Football](https://github.com/vi3itor/GameplayFootball.git): Advanced soccer game. ![From source](./resources/images/icons/source.png)
- [Maldita Castilla](https://portablelinuxgames.org/game/maldita-castilla). Ghost'n'goblings similar game. [Appimage package binary](./resources/images/icons/appimage.png)
- [MIndustry](https://mindustrygame.github.io/). Industry simulator. ![Java 14 binary](./resources/images/icons/java.png) ![Raspian compatible](./resources/images/icons/raspbian.png): A sandbox tower defense game.
- [Minetest](https://www.minetest.net/) with [Mineclone 2](https://malagaoriginal.blogspot.com/2019/03/minetest-con-mineclone-2-en-gnulinux-y.html) included: Open source Minecraft clone. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [RVGL Re-Volt](https://rvgl.re-volt.io/): Re-volt engine. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Step Mania](https://www.stepmania.com/): Dance game. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [VDrift](http://vdrift.net/): Car race game. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [UWOL](https://www.mojontwins.com/juegos_mojonos/uwol-quest-for-money/): Platform game. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### Remakes
- [Abbaye des morts](https://git.disroot.org/nevat/abbayedesmorts-godot/releases): GPL remake made with Godot. ![Pure binary](./resources/images/icons/binary.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Abu Simbel Profanation DeLuxe](https://computeremuzone.com/ficha/598/): Abu Simbel profanation remake. ![Pure binary](./resources/images/icons/binary.png) ![From source](./resources/images/icons/source.png)
- [Digger](http://digger.org): Digger remake. ![Pure binary](./resources/images/icons/binary.png) ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian64.png)
- [pcManiacs](http://www.sromero.org/wiki/spectrum/proyectos/pcmaziacs): Maniacs clone. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Rocks'n'Diamonds](https://www.artsoft.org/rocksndiamonds/): Boulder Dash clone. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [SMK clone](https://github.com/vmbatlle/super-mario-kart): Super Mario Kart clone. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [SMBX clone](https://github.com/Wohlstand/TheXTech): Super Mario Bros clone. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Head over heels](https://github.com/dougmencken/HeadOverHeels): Head over heels remake. [Appimage package binary](./resources/images/icons/appimage.png)
- [Horacio esquiador](https://computeremuzone.com/ficha/710/horacio-esquiador): Horacio esquiador remake. ![Pure binary](./resources/images/icons/binary.png)
- [Hurrican](https://github.com/HurricanGame): Turrican remake. ![From source](./resources/images/icons/source.png)
- [Tanks](https://github.com/krystiankaluzny/Tanks): Tanks game remake. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Temptations](https://github.com/pipagerardo/temptations): Temptations remake. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Vorton, a Highway Encounter remake](https://github.com/zerojay/vorton): Highway encounter remake. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### Java games
- [Abbey of crime extensum](http://www.abadiadelcrimenextensum.com/). Improved Abadia del Crimen remake. ![Java binary](./resources/images/icons/java.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Mars Colonial](http://nosinmipixel.blogspot.com/2014/09/mars-colonial.html): Mars simulator. ![Java 8 binary](./resources/images/icons/java.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [SteamPac](https://sourceforge.net/projects/steampac/): A maze game in 3D. ![Pure binary](./resources/images/icons/binary.png)


### Simplest games
- [Ltris](https://lgames.sourceforge.io/index.php?project=LTris): Tetris clone. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [LPairs 2](http://lgames.sourceforge.net/LPairs/): Paired images game. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### Games for bash console
- [Amurak CBomber](https://github.com/Amunak/CBomber): Bomberman remake for console. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png). Bomberman clone for console.


### Tools for gaming
- [AntiMicroX](https://github.com/AntiMicro/antimicro): Joystick to keyboard mapper. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### Engines with shareware/freeware/gnu data
- [OpenTyrian](https://github.com/opentyrian/opentyrian): Open tyrian engine with game. ![From package](./resources/images/icons/package.png)
- [Rise of the Triad](https://icculus.org/rott): Rise of the triad engine with shareware game. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [The Legend of Zelda: Mystery of Solarus DX](https://www.solarus-games.org/en/games/the-legend-of-zelda-mystery-of-solarus-dx): Zelda games engine, includes one opensource game. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### Network games
- [PuyoVS client](https://github.com/puyonexus/puyovs): PuyoVS network client. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


### Games development
- [Godot Engine](https://godotengine.org/): Great open source engine to make your own games. ![Pure binary](./resources/images/icons/binary.png)
- [LÖVE](https://love2d.org/): Make your own 2D games. ![From package](./resources/images/icons/package.png)
- [LUA](https://ubunlog.com/lua-lenguaje-scripting-ubuntu/): Make your own 2D games. ![From package](./resources/images/icons/package.png)



## TENTE 3D
- [MLCad](http://mlcad.lm-software.com/): LDraw editor (Windows using wine). ![Runs with wine](./resources/images/icons/wine.png)
- [LDView](http://ldview.sourceforge.net/): LDraw viewer (native). ![From source](./resources/images/icons/source.png)
- [Blender](http://www.blender.org) + [Ldraw import addon](https://github.com/TobyLobster/ImportLDraw/releases): Open source 3D creation suite. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [BMP2LDraw](https://www.dropbox.com/s/a82giwfiof15ld5/bmp2ldraw.zip?dl=1): Make labels from BMP files to LDraw pieces. ![Runs with wine](./resources/images/icons/wine.png)
- [LDDesignPad](https://sourceforge.net/projects/lddp): LDraw text editor (Windows using wine).  ![Runs with wine](./resources/images/icons/wine.png)
- [LD4Studio](http://www.ld4dstudio.nl): LDraw animation (Windows using wine).  ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [LDCad](http://www.melkert.net/LDCad): LDraw editor (native).  ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [LDPartEditor](https://nilsschmidt1337.github.io/ldparteditor/). Ldraw pieces editor. ![From source](./resources/images/icons/source.png)
- [LeoCad](https://github.com/leozide/leocad): LDraw editor (native). ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [PovRay Wizard](https://www.dropbox.com/s/bffd8t1mcwxosqu/pov.html?dl=1): Ray-tracing software. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Proa text generator](https://www.dropbox.com/s/rgtxppi4wh6ivcq/proa.html?dl=1): Make bow text for TENTE boats as LDraw pieces.  ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Tube generator](http://tente.webcindario.com/tubo.html): Make TENTE cable pieces as LDraw pieces. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [STL2Dat CP](https://github.com/ondratu/stl2dat-cp): stl2dat cross platform converter. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [StopMotion for Tente 3D](https://www.dropbox.com/s/7iq2ucjaha622xd/stopmotion.html?dl=1): Make stopmotions with TENTE models. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [SVG2LDraw](https://c-mt.dk/software/svg2ldraw/): Convert SVG Images to LDraw Patterns. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)

[TENTE 3D](https://www.dropbox.com/scl/fi/v01a6asba0qvhws8149eo/LDrawTente_Ultima.zip?rlkey=lahysod016goqphwwe7v4xhee&dl=1) & [LEGO 3D](http://www.ldraw.org) supported.
You can add Exin Castillos support [in this web (on Spanish, requires login)](https://exincastillos.es/foro/viewtopic.php?f=87&t=8512).


## Render & 3D
- [FreeCad](https://github.com/FreeCAD/FreeCAD/): CAD software. ![From package](./resources/images/icons/package.png)
- [Goxel](https://goxel.xyz): Voxel editor. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [OpenSCAD](https://www.openscad.org/): CAD software. ![From package](./resources/images/icons/package.png)
- [Wings 3D](http://www.wings3d.com/): CAD software. ![Pure binary](./resources/images/icons/binary.png)



## Educational software
- [Celestia](https://celestia.es/): Virtual planetary. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Free Plane](https://sourceforge.net/projects/freeplane/): Application for Mind Mapping, Knowledge and Project Management. ![Pure java](./resources/images/icons/java.png)
- [Gantt Project](https://descargarganttproject.org/linux/): Gantt diagrams. ![From package](./resources/images/icons/package.png)
- [Linux StopMotion](http://linuxstopmotion.org): Stop motion generator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Lumi H5P](https://github.com/Lumieducation/Lumi): H5P viewer and editor. ![AppImage binary](./resources/images/icons/appimage.png)
- [OpenBoard](https://openboard.ch): Interactive whiteboard for schools and universities. ![From package](./resources/images/icons/package.png)
- [OpenRocket](https://openrocket.info/): Everything you need to design, simulate and fly better rockets ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [OpenToonz](https://github.com/opentoonz/opentoonz): 2D animation software. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Scratch 3](https://scratch.mit.edu/): Lastest Scratch 3 ![From package](./resources/images/icons/package.png)
- [SimulIDE](https://www.simulide.com): Electronics circuit simulator. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)



## Multimedia
- [Audacium](https://github.com/SartoxOnlyGNU/audacium). Audacity fork without telemetry. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Beeper support](https://malagaoriginal.blogspot.com/2019/06/volver-usar-tu-altavoz-interno-con-beep.html). ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [FreeTube](https://github.com/FreeTubeApp/FreeTube): Great Youtube Viewer. ![Appimage package binary](./resources/images/icons/appimage.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [K3B](https://apps.kde.org/es/k3b): Burning CD/DVD/BR software. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [LosslessCut](https://mifi.no/losslesscut/): The Swiss Army Knife of Lossless Video/Audio Editing ![AppImage binary](./resources/images/icons/appimage.png)
- [SpotiFlyer](https://github.com/Shabinder/SpotiFlyer): Download music.![Pure java](./resources/images/icons/java.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Upscayl](https://www.upscayl.org): Free and Open Source AI Image Upscaler. ![AppImage binary](./resources/images/icons/appimage.png)
- [VidCutter](https://github.com/ozmartian/vidcutter/blob/master/vidcutter) ![AppImage binary](./resources/images/icons/appimage.png)
- [VideoLAN with MIDI support](https://wiki.videolan.org/Midi/): The great tool for video (watch, publish, broadcast, convert...). ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Ytfzf](https://github.com/pystardust/ytfzf): CLI video viewer & editor with YouTube support added. [Info en castellano](https://linux-os.net/ytfzf-navega-por-youtube-desde-el-terminal-y-visualiza-los-videos-con-mpv-o-descargalos-con-yt-dlp/). ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian64.png)



## Office automation
- [Haroopad](http://pad.haroopress.com/user.html): Markdown editor. ![From package](./resources/images/icons/package.png)
- [PDFMixTool](https://gitlab.com/scarpetta/pdfmixtool): An application to split, merge, rotate and mix PDF files. ![From source](./resources/images/icons/source.png)
- [Pencil project](https://pencil.evolus.vn/Downloads.html): Make mockups. ![From package](./resources/images/icons/package.png)


## Development (general propouse)
- [Eclipse](https://www.eclipse.org/) ![Pure java](./resources/images/icons/java.png)
- [Free Pascal](https://www.freepascal.org/) with [Lazarus IDE](https://www.lazarus-ide.org/) ![From package](./resources/images/icons/package.png)
- [NetBeans](https://netbeans.apache.org/) ![Pure java](./resources/images/icons/java.png)
- [Rust language](https://www.rust-lang.org): language similar to C++ created by Mozilla Foundation ![From package](./resources/images/icons/package.png)
- [Thonny python IDE for beginners](https://thonny.org/): Python editor IDE. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


## Tools for general propouse
- [Depot MultiSystem](http://liveusb.info/): Make boot USB devices with several systems. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png): Live USB Multiboot.
- [Draw.io](https://app.diagrams.net/): Diagrams editor. ![Pure binary](./resources/images/icons/binary.png)
- [Eggs](https://sourceforge-net.translate.goog/projects/penguins-eggs): Make your own distro. ![From package](./resources/images/icons/package.png)
- [Ghost XPS](https://www.ghostscript.com/download/gxpsdnld.html): XPS generator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Sunflower](https://sunflower-fm.org): Double panel file explorer. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [MultiBootUSB](https://github.com/mbusb/multibootusb): Install several systems in one pendrive. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Raspberry pi imager](https://www.raspberrypi.com/software/): Burn raspberry pi images in microSD in your GNU/Linux PC. ![From package](./resources/images/icons/package.png)
- [Ventoy](https://github.com/ventoy): create bootable USB drive for ISO/WIM/IMG/VHD(x)/EFI files. ![From package](./resources/images/icons/package.png)
- [Tesseract OCR](https://github.com/tesseract-ocr): OCR from console. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


## Network tools
- [Angry IP Scanner](https://angryip.org): Discover devices and computer in your local network. ![From package](./resources/images/icons/package.png)
- [AnyDesk](https://anydesk.com): Remote desktop. ![Pure binary](./resources/images/icons/binary.png)
- [Brave browser](https://brave.com/): Alternative web browser focused in privacy and avoid adventisements. ![Pure binary](./resources/images/icons/binary.png)
- [Ásbrú Connection Manager](https://www.asbru-cm.net): Remote terminal sessions organizer. ![From package](./resources/images/icons/package.png)
- [Marble maps](https://marble.kde.org/): OpenStreetMap viewer. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Google Chrome](https://www.google.com/intl/es_es/chrome): Google Chrome web browser. ![From package](./resources/images/icons/package.png)
- [Rust Desktop](https://github.com/rustdesk/rustdesk): FOSS Remote desktop. ![Pure binary](./resources/images/icons/binary.png)
- [Opera brower](https://www.opera.com): Opera web browser. ![From package](./resources/images/icons/package.png)
- [Team Viewer](https://www.teamviewer.com): Remote desktop. ![Pure binary](./resources/images/icons/binary.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Telegram Desktop](https://desktop.telegram.org/). Tool to see your Telegram in your PC. ![Pure binary](./resources/images/icons/binary.png)
- [Thunderbird](https://www.thunderbird.net). E-mail client. ![From package](./resources/images/icons/package.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [yt-dlp](https://github.com/yt-dlp/yt-dlp). Download multimedia from several sources. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


## Web Apps for general propouse
- [jslogo](https://calormen.com/jslogo): Logo in javascript. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [DesktopCal.js](https://github.com/hvianna/desktopCal.js/): Calendar generator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Easi-Amsword to HTML converter](https://gitlab.com/cpcbegin/easiamsword2html): Converts easy-amsword files in standard HTML ones. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)



## Tools for retro
### Programming
- [CROSS-LIB](https://github.com/Fabrizio-Caruso/CROSS-LIB): retro-hardware abstraction layer for 8 bit coding "universal". ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [CPCTelera (by L.Ronaldo)](https://github.com/lronaldo/cpctelera): Suite for Amstrad CPC programming. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [CPCTools (by Ralferoo)](https://github.com/ralferoo/cpctools) Tools for manage some Amstrad CPC files. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Pasmo Z80 Cross Assembler](http://pasmo.speccy.org/): Z-80 assembler. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [SDCC - Small Device C Compiler](http://sdcc.sourceforge.net/): C compiler. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [SQLYog Community](https://github.com/webyog/sqlyog-community/wiki/Downloads): Avanced SQL client. ![Runs with wine](./resources/images/icons/wine.png)
- [Turbo Rascal Syntax Error](https://lemonspawn.com/turbo-rascal-syntax-error-expected-but-begin): Pascal for several 8 bits systems. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Z88DK - The Development Kit for Z80 Computers](https://github.com/z88dk/z88dk): Z-80 assembler. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [ZX-Editor](https://zx-editor.software.informer.com/): Text editor for ZX-Spectrum emulator files. ![Runs with wine](./resources/images/icons/wine.png)

### Retro media managers
- [CasTools](https://github.com/lunderhage/c64tapedecode): MSX cas tape images converter. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [c64tapedecode](https://github.com/lunderhage/c64tapedecode): Commodore tape images converter.  ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [CPCDiskXP](http://www.cpcmania.com/cpcdiskxp/cpcdiskxp.htm): Amstrad CPC real disks and DSK manager. ![Runs with wine](./resources/images/icons/wine.png)
- [CPCXFS](http://cpctech.cpc-live.com/download.html): Amstrad CPC DSK manager. ![From source](./resources/images/icons/source.png)
- [mtfuji's tap2wav](https://github.com/mtfuji/tap2wav): Manage Sharp X1G tape images. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [NOCart](http://www.cpcwiki.eu/index.php/Nocart): Convert Amstrad DSK images to CPR cartridge images. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Ubercassette](http://www.retroreview.com/iang/UberCassette/): Several 8 bit systems tape converter. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [TAP2Clean](https://sourceforge.net/projects/tapclean/): TAPClean is a Commodore tape preservation / restoration tool. It will check, repair, and remaster Commodore 64 and VIC 20 TAP or DC2N DMP files (tape images). ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [TZX Tools](https://github.com/shred/tzxtools): ZX Spectrum TZX tape files manager. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)

### Other retro tools
- [ConvImgCPC](https://cpcrulez.fr/emulateurs_UTIL-GRA-convimgcpc.htm): Converts images from PC to Amstrad CPC. ![Runs with wine](./resources/images/icons/wine.png)
- [CPCTools (by Ralferoo)](https://github.com/ralferoo/cpctools): Tools for manage Amstrad CPC files. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Martine](https://github.com/jeromelesaux/martine). Converts JPG/PNG files in Amstrad CPC files (compatible with OCP art studio and Impdraw V2). ![Pure binary](./resources/images/icons/binary.png) ![Source avalaible](./resources/images/icons/source.png)
- [Pixel Polizei 2020](https://csdb.dk/release/?id=198001): Graphic editor for export to retrocomputers. ![Pure java](./resources/images/icons/java.png)
- [SplitPather](https://malagaoriginal.blogspot.com/search?updated-max=2021-01-28): Copy files with an extension to a new path with a new structure for easy navigation in gotek, tapuino, etc... ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [UniPixelViewer](https://www.pouet.net/prod.php?which=88808): Converts images between Amstrad CPC and PC (several formats). ![Pure java](./resources/images/icons/java.png) ![Raspian compatible](./resources/images/icons/raspbian.png)

### Retro Languages
- [BBC Basic](https://github.com/rtrussell/BBCSDL): Old BBC computer's Basic. ![From source](./resources/images/icons/source.png)
- [FreeBasic](https://freebasic.net/): Basic compiler for new computers and systems. ![Pure binary](./resources/images/icons/binary.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [QB64](http://www.qb64.net/): Basic compiler for new computers and systems. ![From source](./resources/images/icons/source.png)
- [ZX Basic](https://zxbasic.readthedocs.io): Compile Basic to ZX Spectrum TZX files. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [P65Pas](https://github.com/t-edson/P65Pas): Compile Pascal to Commodore 64 files. ![Pure binary](./resources/images/icons/binary.png) ![From source](./resources/images/icons/source.png)
- [KTurtle Logo](https://edu.kde.org/kturtle/): Logo interpreter. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [UCB Berkeley Logo](https://people.eecs.berkeley.edu/~bh/logo.html): Logo interpreter. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)


#### Legend
![From source](./resources/images/icons/source.png) Compiled from source code.

![From package](./resources/images/icons/package.png) Deb package.

![Pure binary](./resources/images/icons/binary.png) Pure binary with no source code.

![Pure java binary](./resources/images/icons/java.png) Pure java binary with no source code.

![Appimage package binary](./resources/images/icons/appimage.png) App image binary with no source code.

![Run with wine](./resources/images/icons/wine.png) Windows software compatible with GNU/Linux using wine.

![Raspbian compatible](./resources/images/icons/raspbian.png) Raspberry pi compatible.



## How to use
Just type ./retroinstaller.sh and select the desired emulator or tool.

![Emulator category in GNU/Linux](./resources/images/mainmenu.png)



## How to modify
* Own installations script: add your script with a name like *yourscriptkey_installer.sh* in installer folders.
* Menu items: You can add this scripts in the menu using **.menus* files in menu director, first add the yourscriptkey as a word  without spaces, then an space and finally the description than can't contain real spaces only hard spaces.
* Shotcuts: you need to add these files:
   * shotcuts/etc/xdg/menus/applications-merged/yourscriptkey.menu
   * shotcuts/usr/share/applications/yourscriptkey.desktop
   * shotcuts/usr/share/pixmaps/yourscriptkey.png
* Temporal dir to save code and other elements: opt



## About
For more details see my blogs:

- [Selections of M.O.L](https://malagaoriginalenglish.blogspot.com/) (a selection of articles on English).
- [Malagueños Originales y Libres](https://malagaoriginal.blogspot.com) (all the articles on Spanish, more complete).

![Emulator category in GNU/Linux](./resources/images/menu.png)
