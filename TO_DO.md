# Multi retro utils installer

Software I'd like to add to the script in future, if it's possible.
- [plots](https://itsfoss.com/plots-graph-app/)
- [Pyxel](https://github.com/kitao/pyxel): Python engine to make retro games.
- [DAAD](https://github.com/daad-adventure-writer/DRC)
- [GrafX2](http://grafx2.chez.com/)
- [GB Studio](https://www.gbstudio.dev/)
- [PyDance](https://icculus.org/pyddr/).
- [Ancurio/mkxp](https://github.com/Ancurio/mkxp): make games in Rubi.
- [Easyrpg](https://easyrpg.org/)
- [OpenBor](https://github.com/DCurrent/openbor)
- [Cube engine](http://cubeengine.com/)
- [Div Arena](http://div-arena.co.uk/): Div Games Studio evolution. 
- [Prohibition](http://www.indieretronews.com/search/label/javacpc%20games)
- [Maldita Castilla](https://locomalito.com/es/maldita_castilla.php)
- [ZX Editor](https://zx-modules.de/?page_id=46)
- [Miro Video Converter](http://www.mirovideoconverter.com/)
- [Pilas Engine](http://pilas-engine.com.ar/)
- [BennuGD](http://www.bennugd.org/)
- [Cocos2d-x](https://github.com/cocos2d/cocos2d-x)
- [libgdx](https://github.com/libgdx/libgdx): Framework java
- [Gfx2crtc](http://www.cpcwiki.eu/index.php/Gfx2crtc). GFX convert tools to amstrad cpc format.
- [DCVG5K](http://dcvg5k.free.fr): Radiola VG5000 emulator. ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Yorg](https://www.ya2.it/pages/yorg.html): Yorg’s an Open Racing Game
- [best-open-source-games](https://www.slant.co/topics/1933/~best-open-source-games)
- [TI99Sim](https://github.com/billzajac/ti99sim). Texas Instruments TI99 emulator ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [Virtual-Jaguar-Rx](https://github.com/djipi/Virtual-Jaguar-Rx)
- [Educational software](https://malagaoriginal.blogspot.com/2012/12/programas-educativos-para-gnulinux.html)
- [dMagnetic](https://www.dettus.net/dMagnetic/) Magnetic Scrolls emulator ![From source](./resources/images/icons/source.png) ![Raspian compatible](./resources/images/icons/raspbian.png)
- [nvgamepads](https://github.com/miroof/node-virtual-gamepads): Convert your smartphone in a gamepad

Electronic circuit designers:
- [qucs](http://qucs.sourceforge.net/)
- [xcos](https://www.scilab.org/software/xcos)
- [simulide](https://www.simulide.com/p/home.html)
- [boolr](http://boolr.me/)
- [circuitjs1](https://github.com/SEVA77/circuitjs1)
- [geda](http://www.geda-project.org/)
- [ngspice](http://ngspice.sourceforge.net/)
- [a-rostin](http://www.a-rostin.de/)
- [ktechlab](ht(tps://sourceforge.net/projects/ktechlab/)

I'll see this [list of remakes of old games](https://malagaoriginal.blogspot.com/2017/12/remakes-juegos-clasicos-para-linux.html).

