#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/createshortcuts.sh mlcadtente
$pwddir/../commonscripts/createshortcuts.sh mlcadlego
$pwddir/../commonscripts/createshortcuts.sh mlcadexincastillos
source $pwddir/../commonscripts/tenteinit.sh
wget -c http://mlcad.lm-software.com/MLCad_V3.40.zip -O $LDRAWTMP/mlcad.zip
unzip -u $LDRAWTMP/mlcad.zip -d $LDRAWHOME/software/
rm $LDRAWTMP/mlcad.zip
mkdir -p $LDRAWHOME/software/regs
cp -rp $pwddir/../shotcuts/$LDRAWHOME/software/regs/mlcad*.reg $LDRAWHOME/software/regs
mkdir -p $LDRAWHOME/software/linuxscripts
cp -rp $pwddir/../shotcuts$LDRAWHOME/software/linuxscripts/mlcad*.sh $LDRAWHOME/software/linuxscripts