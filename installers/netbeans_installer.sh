#/bin/bash
toolname="netbeans"
toolurl="https://archive.apache.org/dist/netbeans/netbeans-installers/14/Apache-NetBeans-14-bin-linux-x64.sh"
../commonscripts/instalator.sh wget default-jre
../commonscripts/createshortcuts.sh $toolname
wget $toolurl -O $toolname.sh
chmod +x $toolname.sh
./$toolname.sh
rm -f $toolname.sh
