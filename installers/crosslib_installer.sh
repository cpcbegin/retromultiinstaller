#/bin/bash
pwddir=$(pwd)
toolname="crosslib"
toolurl="https://github.com/Fabrizio-Caruso/CROSS-LIB"
setuppath="/opt/linuxemus"
$pwddir/../commonscripts/instalator.sh git python build-essential make ncurses-bin python default-jre
$pwddir/../commonscripts/createshortcuts.sh $toolname
source $pwddir/../commonscripts/makeopenchangetodir.sh $setuppath
$pwddir/../installers/z88_installer.sh
$pwddir/../commonscripts/gitinstaller.sh $toolurl $toolname