#/bin/bash
toolname="rpiimager"
if [ `uname -m` == "x86_64" ]; then
    ../commonscripts/createshortcuts.sh $toolname
    ../commonscripts/instalator.sh wget
    wget -c https://downloads.raspberrypi.org/imager/imager_latest_amd64.deb -O $toolname.deb
    sudo dpkg -i $toolname.deb
    sudo apt install -yf
else
    echo "This version in only for x86-64"
fi
