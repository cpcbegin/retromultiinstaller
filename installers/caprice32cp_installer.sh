#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh git build-essential libsdl1.2-dev libfreetype6-dev libpng-dev zlib1g-dev libsdl2-dev
$pwddir/../commonscripts/createshortcuts.sh caprice32cp
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxemus
$pwddir/../commonscripts/gitinstaller.sh https://github.com/ColinPitrat/caprice32 caprice32cp
cd caprice32cp
git pull
make
sudo make install
cd ..
