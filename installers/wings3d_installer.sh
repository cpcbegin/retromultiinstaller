#/bin/bash
../commonscripts/createshortcuts.sh wings3d
../commonscripts/instalator.sh wget libopengl-dev
wget -c https://downloads.sourceforge.net/project/wings/wings/2.2.6.1/wings-2.2.6.1-linux.bzip2.run?dl=1 -O wings.bzip2.run
sudo chmod +x ./wings.bzip2.run
./wings.bzip2.run
source ../commonscripts/makeopenchangetodir.sh /opt/linuxutils
mv $HOME/wings-2.2.6.1 /opt/linuxutils/wings3d
