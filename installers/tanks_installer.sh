#/bin/bash
pwddir=$(pwd)
toolname="tanks"
toolurl="https://github.com/krystiankaluzny/Tanks"
setuppath="/opt/linuxgames"
$pwddir/../commonscripts/createshortcuts.sh $toolname
$pwddir/../commonscripts/instalator.sh git build-essential libsdl2-dev libsdl2-ttf-dev libsdl2-image-dev
source $pwddir/../commonscripts/makeopenchangetodir.sh $setuppath
$pwddir/../commonscripts/gitinstaller.sh $toolurl $toolname
cd $toolname
make clean all
