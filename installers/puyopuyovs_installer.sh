#/bin/bash
software="puyopuyovs"
pwddir=$(pwd)
#source ../commonscripts/makeopenchangetodir.sh /opt/linuxgames
$pwddir/../commonscripts/createshortcuts.sh $software
$pwddir/../commonscripts/instalator.sh git build-essential gcc cmake make libpulse-dev libasound2-dev libx11-dev qt5-default
$pwddir/../commonscripts/gitinstaller.sh https://github.com/puyonexus/puyovs $software
cd $software
# compilar
git submodule update --init --recursive
cmake -B .build
cmake --build .build --parallel $(nproc)
sudo cmake --install .build