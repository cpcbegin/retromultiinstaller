#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh libsdl2-dev libsdl2-ttf-dev libsdl2-net-dev libsdl2-2.0-0 libsdl2-ttf-2.0-0 libsdl2-net-2.0-0 nasm build-essential
$pwddir/../commonscripts/createshortcuts.sh bbcbasic
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxemus
$pwddir/../commonscripts/gitinstaller.sh https://github.com/rtrussell/BBCSDL bbcbasic
cd bbcbasic/bin/linux
make
