#/bin/bash
toolindex="teamviewer"
../commonscripts/instalator.sh wget
if [ `uname -m` == "x86_64" ]; then
    downloadurl="https://download.teamviewer.com/download/linux/teamviewer_amd64.deb"
elif [ `uname -m` == "i686" ]; then
    downloadurl="https://download.teamviewer.com/download/linux/teamviewer_i386.deb"
elif [[ `uname -m` =~ .*arm.* ]]; then
    downloadurl="https://download.teamviewer.com/download/linux/teamviewer_armhf.deb"
elif [ `uname -m` == "aarch64" ]; then
    downloadurl="https://download.teamviewer.com/download/linux/teamviewer_armhf.deb"
else
    exit ERRCODE "ERROR: TeamViewer doen't support this architecture by the moment."
fi
wget -c $downloadurl -O $toolindex.deb
sudo dpkg -i $toolindex.deb
sudo apt install -fy
