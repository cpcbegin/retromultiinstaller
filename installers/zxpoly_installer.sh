#/bin/bash
../commonscripts/instalator.sh wget unrar default-jre
../commonscripts/createshortcuts.sh zxpoly
source ../commonscripts/makeopenchangetodir.sh /opt/javaemus
wget -c https://github.com/raydac/zxpoly/releases/download/2.3.2/zxpoly-emul-2.3.2-linux-amd64-jdk.tar.gz -O zxpoly.tar.gz
tar zxvf zxpoly.tar.gz
mv -f zxpoly-emul-2.1.1 zxpoly
rm -f zxpoly.tar.gz
