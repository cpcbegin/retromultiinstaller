#/bin/bash
../commonscripts/instalator.sh git build-essential git cmake pkg-config libboost-all-dev qt5-default qtbase5-dev libqt5svg5-dev qtscript5-dev qttools5-dev qttools5-dev-tools libqt5opengl5-dev qtmultimedia5-dev libsuperlu-dev liblz4-dev libusb-1.0-0-dev liblzo2-dev libpng-dev libjpeg-dev libglew-dev freeglut3-dev libfreetype6-dev libjson-c-dev qtwayland5 libqt5multimedia5-plugins libmypaint-dev libgsl23 libopenblas-dev libtiff5-dev
../commonscripts/createshortcuts.sh opentoonz
# libmypaint
../commonscripts/gitinstaller.sh https://github.com/mypaint/libmypaint.git -b v1.3.0
cd libmypaint
./autogen.sh
./configure
make
sudo make install
sudo ldconfig
cd ..
#opentoonz
../commonscripts/gitinstaller.sh https://github.com/opentoonz/opentoonz opentoonz
cd opentoonz
cd thirdparty/tiff-4.0.3
./configure --with-pic --disable-jbig
make -j$(nproc)
cd ../../
cd toonz
mkdir build
cd build
cmake ../sources
make -j$(nproc)
sudo make install
cd ..
mkdir -p $HOME/.config/OpenToonz/stuff/config/qss/Default/
mkdir -p $HOME/.config/OpenToonz/stuff
cp -rp stuff $HOME/.config/OpenToonz/
cat << EOF > $HOME/.config/OpenToonz/SystemVar.ini
[General]
OPENTOONZROOT="$HOME/.config/OpenToonz/stuff"
OpenToonzPROFILES="$HOME/.config/OpenToonz/stuff/profiles"
TOONZCACHEROOT="$HOME/.config/OpenToonz/stuff/cache"
TOONZCONFIG="$HOME/.config/OpenToonz/stuff/config"
TOONZFXPRESETS="$HOME/.config/OpenToonz/stuff/fxs"
TOONZLIBRARY="$HOME/.config/OpenToonz/stuff/library"
TOONZPROFILES="$HOME/.config/OpenToonz/stuff/profiles"
TOONZPROJECTS="$HOME/.config/OpenToonz/stuff/projects"
TOONZROOT="$HOME/.config/OpenToonz/stuff"
TOONZSTUDIOPALETTE="$HOME/.config/OpenToonz/stuff/studiopalette"
EOF
