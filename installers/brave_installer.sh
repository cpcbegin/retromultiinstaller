#/bin/bash
../commonscripts/instalator.sh apt-transport-https curl
if [ -f /etc/apt/sources.list.d/brave-browser-release.list ]; then
    echo "Reposity already added"
elif [ `uname -m` == "x86_64" ]; then
    sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
else
    echo "Architecture not supported."
    exit
fi
sudo apt -y update
sudo apt -y install brave-browser