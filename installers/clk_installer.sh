#/bin/bash
toolindex="clk"
toolurl="https://github.com/TomHarte/CLK"
../commonscripts/instalator.sh git build-essential scons libsdl2-dev zlib1g-dev libopengl-dev mesa-utils
../commonscripts/createshortcuts.sh $toolindex
../commonscripts/gitinstaller.sh $toolurl $toolindex
cd $toolindex
cd OSBindings/SDL
scons
sudo cp -u clksignal /usr/local/bin
sudo mkdir -p /usr/local/share/CLK
sudo cp -r ../../ROMImages/* /usr/local/share/CLK/