#/bin/bash
pwddir=$(pwd)
cp -rp ../shotcuts$LDRAWHOME/software/linuxscripts/ldview*.sh $LDRAWHOME/software/linuxscripts
$pwddir/../commonscripts/createshortcuts.sh ldviewtente
$pwddir/../commonscripts/createshortcuts.sh ldviewlego
$pwddir/../commonscripts/createshortcuts.sh ldviewexincastillos
source $pwddir/../commonscripts/tenteinit.sh
$pwddir/../commonscripts/instalator.sh wget
my_distro=$(lsb_release -ds)
if [[ "$my_distro" == *"Ubuntu 2"* ]]; then
    url='https://github.com/tcobbs/ldview/releases/download/v4.4.1/ldview-qt5-4.4.1-ubuntu-20.04.amd64.deb'
elif [[ "$my_distro" == *"Debian"* ]]; then
    url='https://github.com/tcobbs/ldview/releases/download/v4.5/ldview-qt5-4.5-debian-bullseye.amd64.deb'
else
    url='http://download.opensuse.org/repositories/home:/pbartfai/xUbuntu_19.04/amd64/ldview_4.3_amd64.deb'
fi
echo "Tu distro es $my_distro y se ha descargado LDView de la URL: $url."
sudo apt remove -y ldview
wget -c $url -O $LDRAWTMP/ldview.deb
sudo dpkg -i $LDRAWTMP/ldview.deb
sudo apt install -fy
rm $LDRAWTMP/ldview.deb
mkdir -p $LDRAWHOME/software/linuxscripts