#/bin/bash
pwddir=$(pwd)
toolname="ibmulator"
toolurl="https://github.com/barotto/IBMulator/releases/download/v0.12/ibmulator-0.12-linux.x86_64.tar.gz"
setuppath="/opt/linuxemus/ibmulator"
$pwddir/../commonscripts/instalator.sh wget libsdl2-dev libsdl2-image-dev libglew-dev libarchive-dev libsamplerate-ocaml-dev zlib1g-dev cmake libfreetype-dev
$pwddir/../commonscripts/createshortcuts.sh $toolname
source $pwddir/../commonscripts/makeopenchangetodir.sh $setuppath
wget $toolurl -O $toolname.tar.gz
tar -zxvf $toolname.tar.gz
#toolurl="https://github.com/barotto/IBMulator"
#sudo apt install -y git autotools-dev build-essential libsdl2-dev libsdl2-image-dev libglew-dev libarchive-dev libsamplerate-ocaml-dev zlib1g-dev cmake libfreetype-dev
#../commonscripts/gitinstaller.sh https://github.com/barotto/libRocket.git librocket
#cd librocket/Build
#cmake -G "Unix Makefiles"
#make
#cd ../..
#$pwddir/../commonscripts/gitinstaller.sh $toolurl $toolname
#cd $toolname
#autoreconf --install
#./configure --with-librocket-prefix=../librocket/Build
#make
#sudo make install