#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.shgit build-essential make libsfml-dev python3 libsfml-dev
$pwddir/../commonscripts/createshortcuts.sh smkclone
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxgames
$pwddir/../commonscripts/gitinstaller.sh https://github.com/vmbatlle/super-mario-kart smkclone
cd smkclone/src
make -j8 release
