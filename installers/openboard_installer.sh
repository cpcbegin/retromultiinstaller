#/bin/bash
toolindex="openboard"
if [ `uname -m` == "x86_64" ]; then
    toolurl="https://github.com/OpenBoard-org/OpenBoard/releases/download/v1.6.1/openboard_ubuntu_20.04_1.6.1_amd64.deb"
fi
../commonscripts/instalator.sh wget
wget $toolurl -O $toolindex.deb
sudo dpkg -i $toolindex.deb
sudo apt install -fy
rm -f $toolindex.deb