#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh git cmake build-essential libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-net-dev libsdl2-ttf-dev
$pwddir/../commonscripts/createshortcuts.sh rocksndiamonds
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxgames
$pwddir/../commonscripts/gitinstaller.sh --recurse-submodules https://github.com/tuero/rocksndiamonds
cd rocksndiamonds
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j8
sudo make install