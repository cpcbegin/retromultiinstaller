#/bin/bash
tmppath=$(pwd)
rmipath="`echo "$tmppath" | rev | cut -d/ -f2- | rev`/"
$rmipath/commonscripts/instalator.sh wget
$rmipath/commonscripts/createshortcuts.sh svg2ldraw
source $rmipath/commonscripts/makeopenchangetodir.sh /opt/ldraw/webapps/
$rmipath/commonscripts/gitinstaller.sh https://github.com/LasseD/svg2ldraw