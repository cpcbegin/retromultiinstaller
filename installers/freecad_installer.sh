#/bin/bash
if [ `uname -m` == "x86_64" ]; then
    # Appimage
    utilname="freecad"
    version="1.0.0"
    ../commonscripts/instalator.sh wget
    ../commonscripts/createshortcuts.sh $utilname
    source ../commonscripts/makeopenchangetodir.sh /opt/appimages
    wget -c https://github.com/FreeCAD/FreeCAD/releases/download/$version/FreeCAD_$version-conda-Linux-x86_64-py311.AppImage -O $utilname.AppImage
    chmod +x $utilname.AppImage
else
    # repositorio oficial
    ../commonscripts/instalator.sh freecad
fi

# From source code
#../commonscripts/instalator.sh git build-essential cmake libtool lsb-release python3 swig libboost-dev libboost-date-time-dev libboost-filesystem-dev libboost-graph-dev libboost-iostreams-dev libboost-program-options-dev libboost-python-dev libboost-regex-dev libboost-serialization-dev libboost-thread-dev libcoin-dev libeigen3-dev libgts-bin libgts-dev libkdtree++-dev libmedc-dev libvtk9-dev libx11-dev libxerces-c-dev libyaml-cpp-dev libzipios++-dev libsimage-dev
#../commonscripts/instalator.sh qtbase5-dev qttools5-dev libqt5opengl5-dev libqt5svg5-dev qtwebengine5-dev libqt5xmlpatterns5-dev libqt5x11extras5-dev libpyside2-dev libshiboken2-dev pyside2-tools pyqt5-dev-tools python3-dev python3-matplotlib python3-packaging python3-pivy python3-ply python3-pyside2.qtcore python3-pyside2.qtgui python3-pyside2.qtsvg python3-pyside2.qtwidgets python3-pyside2.qtnetwork python3-pyside2.qtwebengine python3-pyside2.qtwebenginecore python3-pyside2.qtwebenginewidgets python3-pyside2.qtwebchannel pyqt5-dev-tools qt5-default
#../commonscripts/instalator.sh libocct-data-exchange-dev libocct-draw-dev libocct-foundation-dev libocct-modeling-algorithms-dev libocct-modeling-data-dev libocct-ocaf-dev libocct-visualization-dev occt-draw
#git clone --recurse-submodules https://github.com/FreeCAD/FreeCAD.git freecad-source
#cd freecad-source
#mkdir build
#cd build
#cmake ../
#make -j$(nproc --ignore=2)
#sudo make install