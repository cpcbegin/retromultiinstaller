#/bin/bash
toolname="cpcemu"
toolver="2.5"
tooldate="2022-08-13"
if [ `uname -m` == "x86_64" ]; then
    toolurl="https://cpc-emu.org/Release/$tooldate/cpcemu-linux-x86_64-$toolver.tar.gz"
elif [ `uname -m` == "i686" ]; then
    toolurl="https://cpc-emu.org/Release/$tooldate/cpcemu-linux-x86-$toolver.tar.gz"
elif [ `uname -m` == "armv7l" ]; then
    toolurl="https://cpc-emu.org/Release/$tooldate/cpcemu-linux-arm-$toolver.tar.gz"
else
    toolurl="https://cpc-emu.org/Release/$tooldate/cpcemu-linux-arm64-$toolver.tar.gz"
fi
../commonscripts/instalator.sh wget libsdl2-dev
../commonscripts/createshortcuts.sh $toolname
source ../commonscripts/makeopenchangetodir.sh /opt/linuxemus
wget -c $toolurl -O $toolname.tar.gz
tar zxvf $toolname.tar.gz
rm -rf $toolname.tar.gz
mv -f $toolname-$toolver $toolname
cd $toolname
mkdir SDCARD
chmod +x ./cpc464
mkdir -p ~/.CPCemu
cp cpcemu0.cfg ~/.CPCemu/cpcemu.cfg