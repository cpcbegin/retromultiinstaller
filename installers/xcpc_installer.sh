#/bin/bash
pwddir=$(pwd)
toolname="xcpc"
toolurl="https://gitlab.com/ponceto/xcpc"
setuppath="/opt/linuxemus"
initialpath=$PWD
$pwddir/../commonscripts/instalator.sh git build-essential xorg-dev autoconf automake libtool autoconf-archive zlib1g-dev libbz2-dev libgtk-3-dev libmotif-dev
source $pwddir/../commonscripts/makeopenchangetodir.sh $setuppath
$pwddir/../commonscripts/gitinstaller.sh $toolurl $toolname
cd $toolname
cp -rp 
autoreconf -v -i -f
./configure --prefix=$setuppath/$toolname
make -j$(nproc)
sudo make install
cd $initialpath
$pwddir/../commonscripts/createshortcuts.sh $toolname