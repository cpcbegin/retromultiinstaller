#/bin/bash
toolindex="lumi"
../commonscripts/instalator.sh wget dirmngr ca-certificates software-properties-common gnupg gnupg2 apt-transport-https
../commonscripts/createshortcuts.sh lumi
if [ `uname -m` == "x86_64" ]; then
    downloadurl="https://github.com/Lumieducation/Lumi/releases/download/v0.10.0/Lumi-0.10.0.AppImage"
elif [ `uname -m` == "i686" ]; then
    downloadurl="https://github.com/Lumieducation/Lumi/releases/download/v0.10.0/Lumi-0.10.0.AppImage"
elif [[ `uname -m` =~ .*arm.* ]]; then
    downloadurl="https://github.com/Lumieducation/Lumi/releases/download/v0.10.0/Lumi-0.10.0-arm64.AppImage"
elif [ `uname -m` == "aarch64" ]; then
    downloadurl="https://github.com/Lumieducation/Lumi/releases/download/v0.10.0/Lumi-0.10.0-arm64.AppImage"
else
    exit ERRCODE "ERROR: TeamViewer doen't support this architecture by the moment."
fi
source ../commonscripts/makeopenchangetodir.sh /opt/appimages
wget -c $downloadurl -O $toolindex.AppImage
chmod +x $toolindex.AppImage