#/bin/bash
#QB64
../commonscripts/instalator.sh git build-essential g++ libglu1-mesa-dev
sudo mkdir -p /opt/linuxutils
sudo chmod 777 /opt/linuxutils
cd /opt/linuxutils
../commonscripts/gitinstaller.sh https://github.com/QB64Team/qb64
cd qb64
git pull
./setup_lnx.sh
cd ..
../commonscripts/createshortcuts.sh qb64