#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh git build-essential make libsdl2-dev
$pwddir/../commonscripts/createshortcuts.sh sz81
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxemus
$pwddir/../commonscripts/gitinstaller.sh https://github.com/chris-y/sz81 sz81
mv sz81/sz81/* sz81
cd sz81
rmdir sz81
make clean
make
mkdir $HOME/.sz81