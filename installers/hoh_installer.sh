#/bin/bash
pwddir=$(pwd)
gamename="headoverheels"
echo ">>> Installing Head over hells remake..."
$pwddir/../commonscripts/instalator.sh build-essential git cmake 
echo $gamename
$pwddir/../commonscripts/createshortcuts.sh $gamename
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxgames
$pwddir/../commonscripts/gitinstaller.sh https://github.com/dougmencken/HeadOverHeels $gamename
cd $gamename
chmod +x ./linux-build.sh
./linux-build.sh