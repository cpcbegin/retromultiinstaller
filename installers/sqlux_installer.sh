##/bin/bash
destinationpath="/opt/linuxemus/sqlux"
../commonscripts/createshortcuts.sh sqlux
../commonscripts/instalator.sh git build-essential cmake libsdl2-dev
../commonscripts/gitinstaller.sh https://github.com/SinclairQL/sQLux sqlux --recursive
cd sqlux
cmake -B build/
make
mkdir -p $destinationpath
sudo cp -f ./build/sqlux $destinationpath
sudo cp -f ./sqlux.ini $destinationpath
sudo cp -rp ./roms $destinationpath/roms