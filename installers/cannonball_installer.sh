#/bin/bash
#Cannonball Out Run emulator
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh build-essential cmake libsdl2-dev git libboost-dev pip3 libopengl-dev libglu1-mesa-dev
$pwddir/../commonscripts/createshortcuts.sh cannonball
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxemus
#sudo apt remove -y cmake
#sudo pip3 install cmake --upgrade
$pwddir/../commonscripts/gitinstaller.sh --recurse-submodules https://github.com/djyt/cannonball
cd cannonball
git reset --hard b6aa525ddd552f96b43b3b3a6f69326a277206bd
mkdir roms
mkdir build
cd build
cmake -DTARGET:STRING=sdl2 ../cmake
make