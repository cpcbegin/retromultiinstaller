#/bin/bash
if [[ `uname -m` =~ .*x86.* ]]; then
    ../commonscripts/instalator.sh wine wine-stable
    sudo dpkg --add-architecture i386 && sudo apt-get -y update && sudo apt -y install wine32
else
    exit ERRCODE "ERROR: Wine is only supported by x86 or x86_64 architectures."
fi