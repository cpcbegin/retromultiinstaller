#/bin/bash
../commonscripts/instalator.sh git build-essential g++ cmake extra-cmake-modules qttools5-dev qttools5-dev-tools libsdl2-dev libxi-dev libxtst-dev libx11-dev itstool gettext
../commonscripts/gitinstaller.sh https://github.com/AntiMicroX/antimicroX antimicrox
cd antimicrox
git pull
mkdir build
cd build
cmake ..
make
sudo make install