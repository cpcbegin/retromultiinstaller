#/bin/bash
toolname="eclipse"
toolurl="https://ftp.acc.umu.se/mirror/eclipse.org/oomph/epp/2021-03/R/eclipse-inst-jre-linux64.tar.gz"
../commonscripts/instalator.sh wget default-jre
../commonscripts/createshortcuts.sh $toolname
wget $toolurl -O $toolname.tar.gz
tar -zxvf $toolname.tar.gz
cd eclipse-installer
./eclipse-inst