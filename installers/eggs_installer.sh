#/bin/bash
../commonscripts/instalator.sh wget
utilindex="eggs"
if [ `uname -m` == "x86_64" ]; then
    utilurl="https://altushost-swe.dl.sourceforge.net/project/penguins-eggs/DEBS/eggs_9.4.5_amd64.deb"
elif [ `uname -m` == "x86" ]; then
    utilurl="https://altushost-swe.dl.sourceforge.net/project/penguins-eggs/DEBS/eggs_8.17.17-1_i386.deb"
elif [[ `uname -m` =~ .*arm.* ]]; then
    utilurl="https://altushost-swe.dl.sourceforge.net/project/penguins-eggs/DEBS/eggs_9.4.5_armel.deb"
else
    end
fi
wget -c $utilurl -O $utilindex.deb
sudo dpkg -i $utilindex.deb
sudo apt install -f
sudo eggs dad -d
sudo eggs calamares --install