#/bin/bash
../commonscripts/instalator.sh git build-essential gdb qtbase5-dev qttools5-dev-tools qtmultimedia5-dev libqt5multimedia5-plugins libtar-dev libxml2-dev libvorbis-dev pkg-config
../commonscripts/createshortcuts.sh stopmotion
../commonscripts/gitinstaller.sh git://git.code.sf.net/p/linuxstopmotion/code linuxstopmotion
cd linuxstopmotion
qmake -qt=5
sudo make install
sudo cp -u graphics/stopmotion.png /usr/share/pixmaps