#/bin/bash
../commonscripts/instalator.sh git python3 python3-pip
pip3 install jedi setuptools pyserial pylint astroid docutils mypy asttokens send2trash
../commonscripts/gitinstaller.sh https://github.com/thonny/thonny
cd thonny
python3 ./setup.py build
sudo python3 ./setup.py install
sudo cp thonny/res/thonny.png /usr/share/pixmaps/
cd ..
sudo cp -f ../shotcuts/usr/share/applications/thonny.desktop /usr/share/applications/