#/bin/bash
#PCem analisis: https://malagaoriginal.blogspot.com/2018/11/pcem-emulador-de-pcs-antiguos-para.html
../commonscripts/instalator.sh build-essential libsdl1.2-dev libopenal-dev libwxbase3.0-dev libwxgtk3.0-gtk3-dev automake glibc-source wget
mkdir pcem
cd pcem/
wget -c https://pcem-emulator.co.uk/files/PCemV17Linux.tar.gz -O pcemlinux.tar.gz
tar -zxvf pcemlinux.tar.gz 
rm pcemlinux.tar.gz 
./configure
make
sudo make install
cd ..
../commonscripts/createshortcuts.sh pcem
