#/bin/bash
toolindex="freeplane"
toolversion="1.11.8"
toolpath="/opt/javautils/"
toolurl="https://downloads.sourceforge.net/project/freeplane/freeplane%20stable/freeplane_bin-1.11.8.zip?ts=gAAAAABlaPQU6FXDJbL6lmxQoa17x9E7SRhZSsiE1dkg91FmrX2WEsy08MGnE-2r1bdjsAS5h2i6c4Eco9ID88tn5VS4v8vJYw%3D%3D&r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Ffreeplane%2Ffiles%2Ffreeplane%2520stable%2Ffreeplane_bin-1.11.8.zip"
../commonscripts/instalator.sh wget unzip default-jre
../commonscripts/createshortcuts.sh $toolindex
source ../commonscripts/makeopenchangetodir.sh $toolpath
wget -c $toolurl -O $toolindex.zip
unzip -xu $toolindex.zip
chmod +x $toolpath/freemind.sh
rm -f $toolindex.zip
mv $toolindex-$toolversion $toolindex