#/bin/bash
../commonscripts/instalator.sh python3 git make build-essential
../commonscripts/createshortcuts.sh basiliskii
../commonscripts/gitinstaller.sh https://github.com/cebix/macemu
cd macemu/BasiliskII/src/Unix
./autogen.sh
make
sudo make install
