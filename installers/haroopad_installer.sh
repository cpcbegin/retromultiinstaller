#/bin/bash
toolindex="haroopad"
if [ `uname -m` == "x86_64" ]; then
    toolurl="https://bitbucket.org/rhiokim/haroopad-download/downloads/haroopad-v0.13.1-x64.deb"
elif [[ `uname -m` =~ .*x86.* ]]; then
    toolurl="https://bitbucket.org/rhiokim/haroopad-download/downloads/haroopad-v0.13.1-ia32.deb"
fi
../commonscripts/instalator.sh libgconf2-dev
wget $toolurl -O $toolindex.deb
sudo dpkg -i $toolindex.deb
sudo apt install -fy