#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh git cmake libsdl2-dev libsdl2-2.0-0 build-essential libsdl2-image-dev libsdl2-image-2.0-0 libsdl2-mixer-dev libsdl2-mixer-2.0-0
$pwddir/../commonscripts/createshortcuts.sh temptations
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxgames
$pwddir/../commonscripts/gitinstaller.sh https://github.com/pipagerardo/temptations
cd temptations
cd build
cmake ..
make
cp temptations ../bin
cd ../bin
rm -f *.dll
