#/bin/bash
pwddir=$(pwd)
toolname="smbx"
toolurl="https://github.com/Wohlstand/TheXTech"
pathdest="/opt/linuxgames"
pathgame=$HOME"/.PGE_Project/thextech/"
$pwddir/../commonscripts/instalator.sh wget 7z build-essential gcc g++ git cmake ninja-build libsdl2-dev libpng-dev libjpeg-dev
$pwddir/../commonscripts/createshortcuts.sh $toolname
source $pwddir/../commonscripts/makeopenchangetodir.sh $pathdest
$pwddir/../commonscripts/gitinstaller.sh $toolurl $toolname
cd $toolname
git submodule init
git submodule update
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
#cmake -G Ninja -DCMAKE_BUILD_TYPE=Debug ..
#cmake -G Ninja -DCMAKE_BUILD_TYPE=MinSizeRel -DUSE_SYSTEM_SDL2=OFF -DUSE_SYSTEM_LIBS=OFF -DUSE_STATIC_LIBC=ON -DPGE_SHARED_SDLMIXER=OFF ..
make -j$(nproc)
#ninja
mkdir -p $pathgame
cd $pathgame
wget -c https://wohlsoft.ru/TheXTech/_downloads/thextech-smbx13-assets-full.7z
7z x hextech-smbx13-assets-full.7z
rm -f hextech-smbx13-assets-full.7z
wget -c https://wohlsoft.ru/TheXTech/_downloads/thextech-adventure-of-demo-assets-full.7z
7z x thextech-adventure-of-demo-assets-full.7z
rm -f thextech-adventure-of-demo-assets-full.7z