#/bin/bash
../commonscripts/instalator.sh wget python3
../commonscripts/createshortcuts.sh steampac
source ../commonscripts/makeopenchangetodir.sh /opt/linuxgames
wget -c https://downloads.sourceforge.net/project/steampac/linux/SteamPac_1_0_1_linux64.tar.gz?dl=1 -O steampac.tar.gz
tar -zxvf steampac.tar.gz
mv SteamPac_1_0_1_linux64 steampac
rm -f steampac.tar.gz