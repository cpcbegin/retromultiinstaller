#/bin/bash
../commonscripts/instalator.sh wget openjdk-8-jre
lang=$(locale | grep LANGUAGE | cut -d= -f2 | cut -d_ -f1)
gameindex="openrocket"
gameurl="https://github.com/openrocket/openrocket/releases/download/release-15.03/OpenRocket-15.03.jar"
../commonscripts/createshortcuts.sh $gameindex
source ../commonscripts/makeopenchangetodir.sh /opt/javagames/$gameindex
wget -c $gameurl -O $gameindex.jar
