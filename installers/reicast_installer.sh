#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh git build-essential libasound2 libasound2-dev libegl1-mesa-dev libgl1-mesa-dev libgles2-mesa-dev mesa-common-dev libudev-dev
$pwddir/../commonscripts/createshortcuts.sh reicast
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxemus
#sudo apt install -y libpulse-dev
$pwddir/../commonscripts/gitinstaller.sh https://github.com/reicast/reicast-emulator.git reicast
cd reicast/reicast/linux
make
mkdir -p ~/.local/share/reicast/data/
cp $pwddir/../android/assets/buttons.png ~/.local/share/reicast/data/
#cp /path/to/bios/dc_boot.bin ~/.local/share/reicast/data/
#cp /path/to/bios/dc_flash.bin ~/.local/share/reicast/data/