#/bin/bash
backtitle="Multi-installer"
pwddir=$(pwd)
rundir="../installers/"
suffix="_installer.sh"
cd $rundir
opciones=$(ls -1 *$suffix | while read line; do echo ${line/_installer.sh/}; echo ' < off'; done)
cd $pwddir
height=40
width=40
ejecutables=$(dialog --checklist $backtitle $height $width $height $opciones 2>&1 >/dev/tty)
clear
echo $(pwd)
for ejecutable in $ejecutables; do
    $rundir$ejecutable$suffix
done
