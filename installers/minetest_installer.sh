#/bin/bash
echo ">>> Installing Minetest with Mineclone..."
../commonscripts/instalator.sh minetest git
mkdir -p $HOME/.minetest/games
cd $HOME/.minetest/games
../commonscripts/gitinstaller.sh ../commonscripts/gitinstaller.sh http://repo.or.cz/MineClone/MineClone2.git
echo "fullscreen = true" >> $HOME/.minetest/minetest.conf