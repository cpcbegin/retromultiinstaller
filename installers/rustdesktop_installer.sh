#/bin/bash
toolindex="rustdesk"
toolversion="1.1.9"
downloadurl="https://github.com/rustdesk/rustdesk/releases/download"
toolpath="/opt/linuxemus/redream"
if [ `uname -m` == "x86_64" ]; then
    toolurl="$downloadurl/$toolversion/$toolindex-$toolversion.deb"
elif [[ `uname -m` =~ .*arm.* ]]; then
    toolurl="$downloadurl/$toolversion/$toolindex-$toolversion-raspberry-armhf.deb"
else
    end
fi
../commonscripts/instalator.sh wget
../commonscripts/createshortcuts.sh $toolindex
source ../commonscripts/makeopenchangetodir.sh $toolpath
wget -c $toolurl -O $toolindex.deb
sudo dpkg -i $toolindex.deb
sudo apt install -yf
rm -f $toolindex.deb