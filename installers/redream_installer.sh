#/bin/bash
toolindex="redream"
toolpath="/opt/linuxemus/redream"
if [ `uname -m` == "x86_64" ]; then
    toolurl="https://redream.io/download/redream.x86_64-linux-v1.5.0-843-g671f60a.tar.gz"
elif [[ `uname -m` =~ .*arm.* ]]; then
    toolurl="https://redream.io/download/redream.aarch32-raspberry-linux-v1.5.0.tar.gz"
else
    end
fi
../commonscripts/instalator.sh wget
../commonscripts/createshortcuts.sh $toolindex
source ../commonscripts/makeopenchangetodir.sh $toolpath
wget -c $toolurl -O $toolindex.tar.gz
tar -zxvf $toolindex.tar.gz
rm -f $toolindex.tar.gz
