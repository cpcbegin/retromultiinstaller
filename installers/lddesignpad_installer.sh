#/bin/bash
../commonscripts/instalator.sh wget unzip
../commonscripts/createshortcuts.sh lddesignpad
source ../commonscripts/tenteinit.sh
wget -c https://sourceforge.net/projects/lddp/files/LDDP%20Windows%20Binaries/LDDP%202.x/LDDP%202.0.4/LDDP204.zip/download -O $LDRAWTMP/lddesignpad.zip
mkdir $LDRAWHOME/software/lddp
unzip -u $LDRAWTMP/lddesignpad.zip -d $LDRAWHOME/software/lddp
rm $LDRAWTMP/lddesignpad.zip