#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh git cmake build-essential libgl1-mesa-dev libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libsdl2-gfx-dev libopenal-dev libboost-all-dev libdirectfb-dev libst-dev mesa-utils xvfb x11vnc python3-pip
$pwddir/../commonscripts/createshortcuts.sh gpfootball
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxgames
$pwddir/../commonscripts/gitinstaller.sh https://github.com/vi3itor/GameplayFootball.git gameplayfootball
cd gameplayfootball
cp -R data/. build
cd build
cmake ..
make -j$(nproc)