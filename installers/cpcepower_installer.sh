#/bin/bash
toolname="cpcepower"
toolurl="https://www.cpcwiki.eu/forum/emulators/cpcepower-v2101/?action=dlattach;attach=33522"
setuppath="/opt/linuxemus/$toolname"
../commonscripts/instalator.sh wget libsdl2-dev build-essential
../commonscripts/createshortcuts.sh $toolname
source ../commonscripts/makeopenchangetodir.sh $setuppath
wget $toolurl -O $toolname.zip
unzip -ux $toolname.zip
rm -f $toolname.zip