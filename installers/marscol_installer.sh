#/bin/bash
../commonscripts/instalator.sh wget openjdk-8-jre
../commonscripts/createshortcuts.sh marscol
source ../commonscripts/makeopenchangetodir.sh /opt/linuxgames/MarsColonial
wget -c https://downloads.sourceforge.net/project/marscolonial/Linux/MarsColonial-Linux.zip?dl=1 -O marscol.tar.gz
unzip -xu marscol.tar.gz
rm -f marscol.tar.gz