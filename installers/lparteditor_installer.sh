#/bin/bash
pwddir=$(pwd)
toolname="ldparteditor"
$pwddir/../commonscripts/instalator.sh git build-essential ant default-jre
$pwddir/../commonscripts/createshortcuts.sh $toolname
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/ldraw/software
$pwddir/../commonscripts/gitinstaller.sh https://github.com/nilsschmidt1337/ldparteditor $toolname
cd $toolname
ant -noinput -buildfile build-linux.xml