#/bin/bash
../commonscripts/instalator.sh wget
if [ `uname -m` == "x86_64" ]; then
    #../commonscripts/createshortcuts.sh pencilproject
    wget -c https://pencil.evolus.vn/dl/V3.1.0.ga/pencil_3.1.0.ga_amd64.deb -O pencil.deb
elif [ `uname -m` == "i686" ]; then
    #../commonscripts/createshortcuts.sh pencilproject
    wget -c https://pencil.evolus.vn/dl/V3.1.0.ga/pencil_3.1.0.ga_i386.deb -O pencil.deb
else
    echo "ERROR: pencilproject doen't support this architecture by the moment."
    exit
fi
sudo dpkg -i pencil.deb
sudo apt install -f
rm -f pencil.deb
