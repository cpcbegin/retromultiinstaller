#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh git
$pwddir/../commonscripts/createshortcuts.sh retropie
$pwddir/../commonscripts/createshortcuts.sh retropiesetup
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxemus
$pwddir/../commonscripts/gitinstaller.sh https://github.com/RetroPie/RetroPie-Setup.git
cd RetroPie-Setup
chmod +x retropie_setup.sh
sudo ./retropie_setup.sh