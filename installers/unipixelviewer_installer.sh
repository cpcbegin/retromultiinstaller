#/bin/bash
../commonscripts/instalator.sh wget unzip default-jre
lang=$(locale | grep LANGUAGE | cut -d= -f2 | cut -d_ -f1)
utilindex="unipixelviewer"
utilurl="http://cpc-live.com/data/UniPixelViewer.zip"
../commonscripts/createshortcuts.sh $utilindex
source ../commonscripts/makeopenchangetodir.sh /opt/javaemus/$utilindex
wget -c $utilurl -O $utilindex.zip
unzip -xu $utilindex.zip
rm -f $utilindex.zip