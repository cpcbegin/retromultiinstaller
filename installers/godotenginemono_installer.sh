#/bin/bash
toolnameshot="godotengine"
toolname="godotenginemono"
pwddir=$(pwd)
version="4.3"
if [ `uname -m` == "x86_64" ]; then
    arch="x86_64"
elif [ `uname -m` == "i686" ]; then
    arch="x86_32"
elif [ `uname -m` == "aarch64" ]; then
    arch="arm64"
elif [ `uname -m` == "armv7l" ]; then
    arch="arm32"
else
    echo "ERROR: Godot Engine Mono does NOT support this architecture by the moment."
    exit 1
fi
$pwddir/../commonscripts/instalator.sh wget unzip
$pwddir/../commonscripts/createshortcuts.sh $toolname
source ../commonscripts/makeopenchangetodir.sh /opt/linuxutils/$toolname
wget -c https://github.com/godotengine/godot-builds/releases/download/$version-stable/Godot_v$version-stable_mono_linux.${arch}.zip -O $toolnameshort.zip
unzip -xu $toolname.zip
mv Godot* $toolname
chmod +x $toolname
rm -f $toolname.zip
