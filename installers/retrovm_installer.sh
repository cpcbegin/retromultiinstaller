#/bin/bash
toolindex="retrovm"
toolbin="RetroVirtualMachine"
version="2.1.19"
../commonscripts/instalator.sh wget unzip
if [ `uname -m` == "x86_64" ]; then
    downloadurl="https://static.retrovm.org/release/$version/RetroVirtualMachine.$version.Linux.x64.zip"
    ../commonscripts/createshortcuts.sh $toolindex
    echo "Instaling RVM for 64 bits..."
elif [ `uname -m` == "aarch64" ]; then
    downloadurl="https://static.retrovm.org/release/$version/RetroVirtualMachine.$version.Linux.arm64.zip"
    ../commonscripts/createshortcuts.sh $toolindex
    echo "Instaling experimental RVM for Raspberry Pi 4/400/5.."
elif [ `uname -m` == "i686" ]; then
    downloadurl="https://retrovirtualmachine.ams3.digitaloceanspaces.com/release/beta1/linux/x86/RetroVirtualMachine.2.0.beta-1.r7.linux.x86.zip"
    ../commonscripts/createshortcuts.sh $toolindex
    echo "Instaling RVM for 32 bits.."
else
    echo "ERROR: RVM doen't support this architecture by the moment."
    exit
fi
wget $downloadurl -O $toolindex.zip
unzip $toolindex.zip
sudo mv $toolbin /usr/local/bin/
sudo chmod +x /usr/local/bin/$toolbin
../commonscripts/createshortcuts.sh $toolindex
