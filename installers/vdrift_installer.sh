#/bin/bash
../commonscripts/instalator.sh git build-essential g++ scons libsdl2-dev libsdl2-image-dev libbullet-dev libvorbis-dev libcurl4-gnutls-dev
../commonscripts/gitinstaller.sh https://github.com/VDrift/vdrift/ vdrift
cd vdrift
../commonscripts/gitinstaller.sh https://github.com/krichter722/vdrift-data data
scons
sudo scons install
sudo cp vdrift.desktop /usr/share/applications
sudo cp data/textures/icons/vdrift-64x64.png /usr/share/pixmaps/vdrift.png