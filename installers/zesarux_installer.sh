#/bin/bash
software="zesarux"
#carpeta="zesarux" #codigo del raiz del proyecto
carpeta="zesaruxpack" #codigo comprimido zesarux
latestversionurl="http://51.83.33.13/check_updates/stable.txt"
versionpack=$(wget $latestversionurl -q -O -)
if ["$versionpack" == ""]; then
    versionpack="12.0"
fi
echo "ZESarUX $versionpack"
../commonscripts/instalator.sh git build-essential libsdl1.2-dev libsdl2-dev libncurses5-dev libaal-dev libcaca-dev wget
#Desde codigo del raiz del proyecto
#../commonscripts/gitinstaller.sh https://github.com/chernandezba/zesarux
#Desde el código comprimido en releases
wget -c https://github.com/chernandezba/zesarux/releases/download/ZEsarUX-$versionpack/ZEsarUX_src-$versionpack.tar.gz -O $software.tar.gz
tar -zxvf $software.tar.gz
mv ZEsarUX-$versionpack $carpeta
# zesaruxpack para codigo comprimido zesarux para codigo del raiz del proyecto
cd $carpeta
# compilar
git pull
cd src
./configure
make
sudo make install
cd ../..
../commonscripts/createshortcuts.sh zesarux