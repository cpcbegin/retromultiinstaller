#/bin/bash
toolname="pdfmixtool"
# Original repository disabled until supports actual LTS
#toolurl="https://gitlab.com/scarpetta/pdfmixtool"
toolurl="https://github.com/cpcbegin/pdf-mix-tool"
pathdest="/opt/linuxtools"
../commonscripts/instalator.sh git build-essential qt5-qmake cmake libqpdf-dev libqpdf-dev hicolor-icon-theme qtbase5-dev libqt5svg5-dev qttools5-dev qttools5-dev
../commonscripts/createshortcuts.sh eu.scarpetta.PDFMixTool
../commonscripts/gitinstaller.sh $toolurl $toolname
cd $toolname
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release
make
sudo make install