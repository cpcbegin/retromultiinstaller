#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh git qt5-qmake build-essential
$pwddir/../commonscripts/createshortcuts.sh virtualjaguar
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxemus
$pwddir/../commonscripts/gitinstaller.sh https://github.com/mirror/virtualjaguar
cd virtualjaguar
./configure
make
sudo make install