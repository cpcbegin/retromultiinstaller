#/bin/bash
../commonscripts/instalator.sh wget
utilname="tapclean"
utilpath=""
if [ `uname -m` == "x86_64" ]; then
    utilurl="https://downloads.sourceforge.net/project/tapclean/tapclean/TAPClean%200.38/tapclean-0.38-linux-x86_64.gz"
elif [ `uname -m` == "x86" ]; then
    utilurl="https://downloads.sourceforge.net/project/tapclean/tapclean/TAPClean%200.29/tapclean-0.29-linux.gz"
elif [[ `uname -m` =~ .*arm.* ]]; then
    utilurl="https://downloads.sourceforge.net/project/tapclean/tapclean/TAPClean%200.38/tapclean-0.38-linux-armv7l.gz"
else
    end
fi
wget -c $utilurl -O $utilname.gz
gunzip -f $utilname.gz
sudo chmod +x $utilname
sudo cp -u $utilname /usr/local/bin