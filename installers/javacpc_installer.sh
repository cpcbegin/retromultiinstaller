#/bin/bash
../commonscripts/instalator.sh wget unzip default-jre
../commonscripts/createshortcuts.sh javacpc
source ../commonscripts/makeopenchangetodir.sh /opt/javaemus/javacpc
wget -c https://downloads.sourceforge.net/project/javacpc/JavaCPC%20Desktop/JavaCPC_Desktop_3.0.2.zip -O javacpc.zip
unzip -xu javacpc.zip
rm -f javacpc.zip