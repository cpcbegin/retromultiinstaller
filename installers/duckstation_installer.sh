#/bin/bash
toolname="duckstation"
toolurl="https://github.com/stenzek/duckstation/releases/download/latest/duckstation-qt-x64.AppImage"
pathdest="/opt/appimages"
../commonscripts/instalator.sh wget
../commonscripts/createshortcuts.sh $toolname
source ../commonscripts/makeopenchangetodir.sh $pathdest
wget -c $toolurl
chmod +x duckstation-qt-x64.AppImage