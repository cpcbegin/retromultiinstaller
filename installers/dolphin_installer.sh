#/bin/bash
../commonscripts/instalator.sh build-essential git cmake ffmpeg libavcodec-dev libavformat-dev libavutil-dev libswscale-dev libevdev-dev libusb-1.0-0-dev libxrandr-dev libxi-dev libpangocairo-1.0-0 qt6-base-private-dev libqt6svg6-dev libbluetooth-dev libasound2-dev libpulse-dev libgl1-mesa-dev libcurl4-openssl-dev libudev-dev libsystemd-dev || sudo apt install libeudev-dev #distros without systemd like AntiX must use eudev instead
../commonscripts/createshortcuts.sh dolphin
../commonscripts/gitinstaller.sh https://github.com/dolphin-emu/dolphin
cd dolphin
git submodule update --init --recursive \
Externals/mGBA \
Externals/spirv_cross \
Externals/zlib-ng \
Externals/libspng \
Externals/VulkanMemoryAllocator \
Externals/cubeb \
Externals/implot \
Externals/gtest \
Externals/rcheevos \
&& git pull --recurse-submodules
mkdir build
cd build
cmake ..
make -j$(nproc)
sudo make install
rm -f $HOME/.local/share/applications/dolphin-emu.desktop
#https://www.gamingonlinux.com/forum/topic/3247/
sudo cp -r ../Data/51-usb-device.rules /etc/udev/rules.d
sudo udevadm control --reload-rules