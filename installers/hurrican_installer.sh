#/bin/bash
pwddir=$(pwd)
GAME=hurrican
$pwddir/../commonscripts/instalator.sh git wget build-essential cmake libsdl2-dev libsdl2-image-dev libsdl2-dev libsdl2-mixer-dev libepoxy-dev
$pwddir/../commonscripts/createshortcuts.sh $GAME
#sudo apt install libsdl1.2-dev
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxgames
$pwddir/../commonscripts/gitinstaller.sh --recurse-submodules https://github.com/HurricanGame/Hurrican.git
cd Hurrican/Hurrican
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
mv hurrican ..