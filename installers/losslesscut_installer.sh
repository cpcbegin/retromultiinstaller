#/bin/bash
../commonscripts/instalator.sh wget
../commonscripts/createshortcuts.sh losslesscut
source ../commonscripts/makeopenchangetodir.sh /opt/appimages
wget -c https://github.com/mifi/lossless-cut/releases/download/v3.55.2/LosslessCut-linux-x86_64.AppImage -O LosslessCut.AppImage
chmod +x LosslessCut.AppImage