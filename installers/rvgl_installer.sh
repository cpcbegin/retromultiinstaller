#/bin/bash
echo ">>> Installing RVGL Revolt remake..."
../commonscripts/instalator.sh python3
../commonscripts/createshortcuts.sh rvgl
source ../commonscripts/makeopenchangetodir.sh /opt/linuxgames/rvgl
wget -c https://rvgl.re-volt.io/downloads/install_rvgl.py
python3 ./install_rvgl.py install