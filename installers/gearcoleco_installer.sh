#/bin/bash
pwddir=$(pwd)
toolname="gearcoleco"
toolurl="https://github.com/drhelius/Gearcoleco"
$pwddir/../commonscripts/instalator.sh git build-essential libsdl2-dev libglew-dev
$pwddir/../commonscripts/createshortcuts.sh gearcoleco
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxemus
$pwddir/../commonscripts/gitinstaller.sh $toolurl $toolname
cd $toolname
cd platforms/linux
make -j`nproc`