#/bin/bash
toolname="sqlyog"
toolurl="hhttps://s3.amazonaws.com/SQLyog_Community/SQLyog+13.1.5/SQLyog-13.1.5-0.x86Community.exe"
../commonscripts/instalator.sh wget
../installers/wine_installer.sh
../commonscripts/createshortcuts.sh $toolname
source ../commonscripts/makeopenchangetodir.sh $pathdest
wget $toolurl -O $toolname.exe
wine ./$toolname.exe