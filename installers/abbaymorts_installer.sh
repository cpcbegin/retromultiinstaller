#/bin/bash
gamelabel="abbaymorts"
gamefullname="abbayedesmorts-godot"
version="1.1.0"
arch=`uname -m`
../commonscripts/instalator.sh wget unzip
if [ $arch == "x86_64" ]; then
    downloadurl="https://git.disroot.org/nevat/abbayedesmorts-godot/releases/download/$version/abbayedesmorts-godot-1.1.0-linux-$arch"
elif [ $arch == "i686" ]; then
    downloadurl="https://git.disroot.org/nevat/abbayedesmorts-godot/releases/download/1.1.0/abbayedesmorts-godot-1.1.0-linux-x86_32"
elif [ $arch == "aarch64" ]; then
    downloadurl="https://git.disroot.org/nevat/abbayedesmorts-godot/releases/download/1.1.0/abbayedesmorts-godot-1.1.0-linux-arm64"
elif [ $arch == "armv7l" ]; then
    downloadurl="https://git.disroot.org/nevat/abbayedesmorts-godot/releases/download/1.1.0/abbayedesmorts-godot-1.1.0-linux-x86_32"
else
    echo "ERROR: $gamefullname doen't support this architecture by the moment."
    exit
fi
../commonscripts/createshortcuts.sh $gamelabel
wget $downloadurl -O $gamelabel
chmod +x $gamelabel
sudo mv -f $gamelabel /usr/local/bin/
../commonscripts/createshortcuts.sh $gamelabel
