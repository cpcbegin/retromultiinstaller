#/bin/bash
toolname="telegram"
toolurl="https://telegram.org/dl/desktop/linux"
pathdest="/opt/linuxutils"
../commonscripts/instalator.sh wget xz-utils
../commonscripts/createshortcuts.sh $toolname
source ../commonscripts/makeopenchangetodir.sh $pathdest
wget $toolurl -O $toolname.tar.xz
tar -xf $toolname.tar.xz
chmod +x Telegram/Telegram
rm -f $toolname.tar.xz