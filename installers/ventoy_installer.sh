#/bin/bash
toolname="ventoy"
toolversion="1.0.80"
toolurl="https://github.com/ventoy/Ventoy/releases/download/v$toolversion/ventoy-$toolversion-linux.tar.gz"
../commonscripts/instalator.sh wget
../commonscripts/createshortcuts.sh $toolname
source ../commonscripts/makeopenchangetodir.sh /opt/linuxutils
wget $toolurl -O $toolname.tar.gz
tar -zxvf $toolname.tar.gz
mv $toolname-$toolversion $toolname