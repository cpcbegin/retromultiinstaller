#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh git
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxemus/xroar/coco
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxemus/xroar/dragon
$pwddir/../commonscripts/createshortcuts.sh xroarcoco
$pwddir/../commonscripts/createshortcuts.sh xroardragon
$pwddir/../commonscripts/instalator.sh git build-essential make gawk libsdl2-dev libsndfile1-dev libgtk2.0-dev libgtkglext1-dev libasound2-dev
$pwddir/../commonscripts/gitinstaller.sh http://www.6809.org.uk/git/xroar.git/
cd xroar
./autogen.sh
./configure
make -B
sudo make install