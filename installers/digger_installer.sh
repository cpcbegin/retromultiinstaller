#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/createshortcuts.sh digger
$pwddir/../commonscripts/instalator.sh build-essential libsdl2-image-dev libsdl2-mixer-dev git make cmake
#source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxgames
git clone https://github.com/sobomax/digger
cd digger
mkdir build
cd build
cmake ..
make
sudo cp -f digger /usr/local/bin/