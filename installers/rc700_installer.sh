#/bin/bash
toolindex="rc700"
toolpath="/opt/linuxemus/rc700"
toolurl="http://www.jbox.dk/rc702/rc700-src.zip"
../commonscripts/instalator.sh wget build-essential
../commonscripts/createshortcuts.sh $toolindex
source ../commonscripts/makeopenchangetodir.sh $toolpath
wget -c http://www.jbox.dk/rc702/rc700-src.zip -O $toolindex.zip
unzip -xu $toolindex.zip
make
sudo cp -rp $toolpath/rc-logo.ico /usr/share/pixmaps
#rm $toolindex.zip