#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh git libqt4-dev zlib1g-dev build-essential
$pwddir/../commonscripts/createshortcuts.sh leocadtente
$pwddir/../commonscripts/createshortcuts.sh leocadlego
$pwddir/../commonscripts/createshortcuts.sh leocadexincastillos
source $pwddir/../commonscripts/tenteinit.sh
$pwddir/../commonscripts/gitinstaller.sh https://github.com/leozide/leocad/
cd leocad
qmake leocad.pro
make
sudo make install
