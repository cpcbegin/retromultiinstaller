#/bin/bash
toolname="trse"
toolurl="https://github.com/leuat/TRSE/releases/download/nightly/trse_linux64.tar.gz"
pathdest="/opt/linuxemus"
../commonscripts/instalator.sh wget libqt5dbus5 libqt5widgets5 libqt5network5 libqt5gui5 libqt5core5a libxcb-xinerama0
../commonscripts/createshortcuts.sh $toolname
source ../commonscripts/makeopenchangetodir.sh $pathdest
wget $toolurl -O $toolname.tar.gz
tar -zxvf $toolname.tar.gz
rm -f $toolname.tar.gz
