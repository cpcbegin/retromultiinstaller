#/bin/bash
../commonscripts/instalator.sh build-essential zlib1g-dev libcurl4 libzip-dev libsdl1.2-dev libsdl-image1.2-dev libcurl4-gnutls-dev imagemagick libsdl2-image-dev libcurl4-gnutls-dev libzip-dev
../commonscripts/createshortcuts.sh linapple
../commonscripts/gitinstaller.sh https://github.com/linappleii/linapple
cd linapple
make
sudo make install