#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh git scons pkg-config libglfw3-dev libgtk-3-dev
$pwddir/../commonscripts/createshortcuts.sh goxel
source $pwddir/../commonscripts/makeopenchangetodir.sh /opt/linuxutils
$pwddir/../commonscripts/gitinstaller.sh https://github.com/guillaumechereau/goxel
cd goxel
make release