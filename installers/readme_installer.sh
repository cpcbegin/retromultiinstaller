#/bin/bash
if [ -f /usr/bin/okular ]; then
    okular ../README.md
else
    sensible-browser https://gitlab.com/cpcbegin/retromultiinstaller/-/blob/master/README.md
fi
