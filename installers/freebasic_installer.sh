#/bin/bash
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh wget build-essential gcc libncurses5-dev libffi-dev libgl1-mesa-dev libx11-dev libxext-dev libxrender-dev libxrandr-dev libxpm-dev libtinfo5 libgpm-dev
utilindex="freebasic"
toolpath="/opt/linuxutils/$utilindex"
architech=`uname -m`
$pwddir/../commonscripts/instalator.sh git build-essential libsdl2-dev libglew-dev
if [ $architech == "x86_64" ]; then
    fullname="FreeBASIC-1.10.1-ubuntu-22.04-${architech}"
    utilurl="https://altushost-swe.dl.sourceforge.net/project/fbc/FreeBASIC-1.10.1/Binaries-Linux/${fullname}.tar.gz"
elif [ $architech == "x86" ]; then
    fullname="FreeBASIC-1.10.1-ubuntu-18.04-${architech}"
    utilurl="https://altushost-swe.dl.sourceforge.net/project/fbc/FreeBASIC-1.10.1/Binaries-Linux/${fullname}.tar.gz"
elif [[ $architech =~ .*arm.* ]]; then
    fullname="FreeBASIC-1.10.1-ubuntu-22.04-${architech}"
    utilurl="https://altushost-swe.dl.sourceforge.net/project/fbc/FreeBASIC-1.10.1/Binaries-Linux/${fullname}.tar.gz"
else
    end
fi
wget -c $utilurl -O $utilindex.tar.gz
tar -zxvf $utilindex.tar.gz
mv $fullname $utilindex
cd $utilindex
sudo ./install.sh -i