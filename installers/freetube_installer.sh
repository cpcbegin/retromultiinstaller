#/bin/bash
toolname="freetube"
pwddir=$(pwd)
version="0.23.2"
if [ `uname -m` == "x86_64" ]; then
    arch="amd64"
elif [ `uname -m` == "aarch64" ]; then
    arch="arm64"
elif [ `uname -m` == "armv7l" ]; then
    arch="armv7l"
else
    echo "ERROR: Freetube does NOT support this architecture by the moment."
    exit 1
fi
$pwddir/../commonscripts/instalator.sh wget unzip
$pwddir/../commonscripts/createshortcuts.sh $toolname
source ../commonscripts/makeopenchangetodir.sh /opt/appimages
wget -c https://github.com/FreeTubeApp/FreeTube/releases/download/v${version}-beta/freetube-${version}-${arch}.AppImage -O $toolname.AppImage
chmod +x $toolname.AppImage
