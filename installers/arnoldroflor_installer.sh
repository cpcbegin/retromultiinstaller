#/bin/bash
pwddir=$(pwd)
$pwddir/.../commonscripts/instalator.sh git build-essential libgtk2.0-dev libsdl1.2-dev libx11-dev
$pwddir/.../commonscripts/createshortcuts.sh arnoldrofl0r /opt/linuxemus
source $pwddir/.../commonscripts/makeopenchangetodir.sh /opt/linuxemus
$pwddir/.../commonscripts/gitinstaller.sh https://github.com/rofl0r/arnold arnoldrofl0r
cd arnoldrofl0r
git pull
cd src
./autogen.sh
./configure
make
