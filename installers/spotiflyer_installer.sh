#/bin/bash
../commonscripts/instalator.sh wget unrar default-jre
gameindex="spotiflyer"
gameurl="https://github.com/Shabinder/SpotiFlyer/releases/download/v3.6.1/SpotiFlyer-linux-x64-3.6.1.jar"
../commonscripts/createshortcuts.sh $gameindex
source ../commonscripts/makeopenchangetodir.sh /opt/javautils/$gameindex
wget -c $gameurl -O $gameindex.jar