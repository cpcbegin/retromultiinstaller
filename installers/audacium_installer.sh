#/bin/bash
#https://blog.desdelinux.net/audacium-el-fork-de-audacity-sin-telemetria/
toolname="audacium"
toolurl="https://github.com/SartoxOnlyGNU/audacium"
../commonscripts/instalator.sh git build-essential cmake git python3-pip
sudo pip3 install conan
sudo apt install -y libgtk2.0-dev libasound2-dev libavformat-dev libjack-jackd2-dev uuid-dev
../commonscripts/createshortcuts.sh $toolname
../commonscripts/gitinstaller.sh $toolurl $toolname
cd $toolname
mkdir build
cd build
cmake -G "Unix Makefiles" -Daudacity_use_ffmpeg=loaded ../
make -j`nproc`
sudo make install