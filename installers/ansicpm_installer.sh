#/bin/bash
#CP/M emulator
../commonscripts/instalator.sh git build-essential make
../commonscripts/gitinstaller.sh https://github.com/z88dk/cpm
cd cpm
make
sudo mv ./cpm /usr/local/bin/ansicpm
sudo mv ./cpmtool /usr/local/bin/ansicpmtool