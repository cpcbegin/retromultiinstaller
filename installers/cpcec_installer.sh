#/bin/bash
#CPCEC Emulator
pwddir=$(pwd)
$pwddir/../commonscripts/instalator.sh build-essential libsdl1.2-dev libsdl2-dev gcc wget git
$pwddir/../commonscripts/createshortcuts.sh cpcec
$pwddir/../commonscripts/createshortcuts.sh zxsec
$pwddir/../commonscripts/createshortcuts.sh csfec
$pwddir/../commonscripts/createshortcuts.sh msxec
source ../commonscripts/makeopenchangetodir.sh /opt/linuxemus
$pwddir/../commonscripts/gitinstaller.sh https://github.com/cpcitor/cpcec
cd cpcec
for original in *; do mv $original `echo $original | tr '[:upper:]' '[:lower:]'`; done
emulators=(cpcec zxsec xrf csfec msxec runec)
for i in "${emulators[@]}"
do
    echo "** Compiling $i, please wait... **"
    gcc -fsigned-char -DDEBUG -DSDL2 -O2 -xc $i.c -lSDL2 -o$i
    chmod +x $i
done
#chmod +x !(*.*)
