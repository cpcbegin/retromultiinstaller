#/bin/bash
if [ -n "$1" ]; then
    ../commonscripts/instalator.sh git
    origin="$1"
    if [ -n "$2" ]; then
        destination="$2"
    else
        destination=$(echo "$1" | rev | cut -d"/" -f1 |rev)
    fi
    if [ -n "$3" ]; then
        options="$3"
    else
        options=""
    fi
    if [ -d "$destination" ]; then
        cd "$destination"
        git pull
    else
        git clone $options $origin $destination
    fi
else
    exit ERRCODE "ERROR: No parameters found."
fi