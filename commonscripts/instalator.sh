#/bin/bash
comando="sudo apt -y install"
if [[ "$#" -gt 0 ]]; then
    for param in "$@"; do
        $comando $param
    done
else
    exit ERRCODE "ERROR: No parameters found."
fi